<?php
/**
 * Curse Inc.
 *
 * @package   HydraWiki
 * @author    Samuel Hilson <shhilson@curse.com>
 * @copyright 2019 Curse, inc.
 * @license   MIT
 */

namespace Tests\Unit\Commenting;

use HydraWiki\Sniffs\WhiteSpace\FunctionSpacingSniff;
use PHP_CodeSniffer\Tokenizers\PHP;
use Test\BaseTest;

class FunctionSpacingSniffTest extends BaseTest {
	/**
	 * Container for sniff under test
	 *
	 * @var FunctionSpacingSniff
	 */
	protected $sniff;

	/**
	 * Setup sniff
	 *
	 * @return void
	 */
	protected function setUp() : void {
		parent::setUp();

		$this->sniff = new FunctionSpacingSniff();
	}

	/**
	 * Test sniff with a good function spacing
	 *
	 * @covers FunctionSpacingSniff::process
	 * @return void
	 */
	public function testProcessWithGoodFunctionsSpacing() {
		$file = file_get_contents(__DIR__ . '/../../Files/FunctionSpacing/GoodFunctionSpacing.php');
		$tokenizer = new PHP($file, $this->config, "\n");
		$tokens = $tokenizer->getTokens();
		$this->fileMock->numTokens = count($tokens);

		$this->fileMock->method('getTokens')
			->willReturn($tokens);

		$this->fileMock->method('findNext')
			->will(
				$this->returnCallback(
					function (...$args) use ($tokens) {
						return $this->findNext($args, $tokens);
					}
				)
			);
		$this->fileMock->method('findPrevious')
			->will(
				$this->returnCallback(
					function (...$args) use ($tokens) {
						return $this->findPrevious($args, $tokens);
					}
				)
			);
		$this->fileMock->expects($this->exactly(0))
			->method('addFixableError');

		$result = $this->sniff->process($this->fileMock, 79);

		$this->assertNull($result);
	}

	/**
	 * Test sniff with a bad function spacing
	 *
	 * @covers FunctionSpacingSniff::process
	 * @return void
	 */
	public function testProcessWithBadFunctionsSpacing() {
		$file = file_get_contents(__DIR__ . '/../../Files/FunctionSpacing/BadFunctionSpacing.php');
		$tokenizer = new PHP($file, $this->config, "\n");
		$tokens = $tokenizer->getTokens();
		$this->fileMock->numTokens = count($tokens);

		$this->fileMock->method('getTokens')
			->willReturn($tokens);

		$this->fileMock->method('findNext')
			->will(
				$this->returnCallback(
					function (...$args) use ($tokens) {
						return $this->findNext($args, $tokens);
					}
				)
			);
		$this->fileMock->method('findPrevious')
			->will(
				$this->returnCallback(
					function (...$args) use ($tokens) {
						return $this->findPrevious($args, $tokens);
					}
				)
			);
		$this->fileMock->expects($this->exactly(2))
			->method('addFixableError');

		$result = $this->sniff->process($this->fileMock, 80);

		$this->assertNull($result);
	}
}
