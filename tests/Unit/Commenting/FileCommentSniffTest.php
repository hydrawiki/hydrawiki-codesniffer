<?php
/**
 * Curse Inc.
 *
 * @package   HydraWiki
 * @author    Samuel Hilson <shhilson@curse.com>
 * @copyright 2019 Curse, inc.
 * @license   MIT
 */

namespace Tests\Unit\Commenting;

use HydraWiki\Sniffs\Commenting\FileCommentSniff;
use PHP_CodeSniffer\Tokenizers\PHP;
use Test\BaseTest;

class FileCommentSniffTest extends BaseTest {
	/**
	 * Container for sniff under test
	 *
	 * @var FileCommentSniff
	 */
	protected $sniff;

	/**
	 * Setup sniff
	 *
	 * @return void
	 */
	protected function setUp() : void {
		parent::setUp();

		$this->sniff = new FileCommentSniff();
	}

	/**
	 * Test sniff with a good file comment
	 *
	 * @covers FileCommentSniff::process
	 * @return void
	 */
	public function testProcessWithGoodFileComment() {
		$file = file_get_contents(__DIR__ . '/../../Files/FileComment/GoodFileComment.php');
		$tokenizer = new PHP($file, $this->config, "\n");
		$tokens = $tokenizer->getTokens();

		$this->fileMock->method('getTokens')
			->willReturn($tokens);

		$this->fileMock->method('findNext')
			->will(
				$this->returnCallback(
					function (...$args) use ($tokens) {
						return $this->findNext($args, $tokens);
					}
				)
			);

		$this->fileMock->expects($this->exactly(0))
			->method('addFixableError');

		$this->fileMock->expects($this->exactly(0))
			->method('addError');

		$result = $this->sniff->process($this->fileMock, 0);

		$this->assertEquals($result, 1);
	}

	/**
	 * Test sniff with Bad file comment
	 *
	 * @covers FileCommentSniff::process
	 * @return void
	 */
	public function testProcessWithBadFileComment() {
		$file = file_get_contents(__DIR__ . '/../../Files/FileComment/BadFileComment.php');
		$tokenizer = new PHP($file, $this->config, "\n");
		$tokens = $tokenizer->getTokens();

		$this->fileMock->method('getTokens')
			->willReturn($tokens);

		$this->fileMock->method('findNext')
			->will(
				$this->returnCallback(
					function (...$args) use ($tokens) {
						return $this->findNext($args, $tokens);
					}
				)
			);

		$this->fileMock->expects($this->exactly(2))
			->method('addFixableError');

		$this->fileMock->expects($this->exactly(0))
			->method('addError');

		$result = $this->sniff->process($this->fileMock, 0);

		$this->assertEquals($result, 1);
	}
}
