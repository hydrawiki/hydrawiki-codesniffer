<?php
/**
 * Curse Inc.
 *
 * @package   HydraWiki
 * @author    Samuel Hilson <shhilson@curse.com>
 * @copyright 2019 Curse, inc.
 * @license   MIT
 */

namespace Tests\Unit\Commenting;

use HydraWiki\Sniffs\Commenting\InlineCommentSniff;
use PHP_CodeSniffer\Tokenizers\PHP;
use Test\BaseTest;

class InlineCommentSniffTest extends BaseTest {
	/**
	 * Container for sniff under test
	 *
	 * @var InlineCommentSniff
	 */
	protected $sniff;

	/**
	 * Setup sniff
	 *
	 * @return void
	 */
	protected function setUp() : void {
		parent::setUp();

		$this->sniff = new InlineCommentSniff();
	}

	/**
	 * Test sniff with a good inline comment
	 *
	 * @covers InlineCommentSniff::process
	 * @return void
	 */
	public function testProcessWithGoodInlineComment() {
		$file = file_get_contents(__DIR__ . '/../../Files/InlineComment/GoodInlineComment.php');
		$tokenizer = new PHP($file, $this->config, "\n");
		$tokens = $tokenizer->getTokens();

		$this->fileMock->method('getTokens')
			->willReturn($tokens);

		$this->fileMock->method('findPrevious')
			->will(
				$this->returnCallback(
					function (...$args) use ($tokens) {
						return $this->findPrevious($args, $tokens);
					}
				)
			);

		$this->fileMock->expects($this->exactly(0))
			->method('addFixableError');

		$this->fileMock->expects($this->exactly(0))
			->method('addError');

		$result = $this->sniff->process($this->fileMock, 1);

		$this->assertNull($result);
	}

	/**
	 * Test sniff with Bad inline comment
	 *
	 * @covers FileCommentSniff::process
	 * @return void
	 */
	public function testProcessWithBadInlineComment() {
		$file = file_get_contents(__DIR__ . '/../../Files/InlineComment/BadInlineComment.php');
		$tokenizer = new PHP($file, $this->config, "\n");
		$tokens = $tokenizer->getTokens();

		$this->fileMock->method('getTokens')
			->willReturn($tokens);

		$this->fileMock->method('findPrevious')
			->will(
				$this->returnCallback(
					function (...$args) use ($tokens) {
						return $this->findPrevious($args, $tokens);
					}
				)
			);

		$this->fileMock->expects($this->exactly(1))
			->method('addFixableWarning');

		$result = $this->sniff->process($this->fileMock, 1);

		$this->assertNull($result);
	}
}
