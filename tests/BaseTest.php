<?php
/**
 * Curse Inc.
 *
 * @package   HydraWiki
 * @author    Samuel Hilson <shhilson@curse.com>
 * @copyright 2019 Curse, inc.
 * @license   MIT
 */

namespace Test;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Fixer;
use PHPUnit\Framework\TestCase;
use stdClass;

class BaseTest extends TestCase {
	/**
	 * Container for the File Mock class
	 *
	 * @var File
	 */
	protected $fileMock;

	/**
	 * Config object
	 *
	 * @var object
	 */
	protected $config;

	/**
	 * Tokens from $tokenizer
	 *
	 * @var array
	 */
	protected $tokens;

	/**
	 * Setup the mock classes for test
	 *
	 * @return void
	 */
	protected function setUp() : void {
		if (defined('PHP_CODESNIFFER_VERBOSITY') === false) {
			define('PHP_CODESNIFFER_VERBOSITY', 0);
		}
		$this->createConfig();
		$this->fileMock = $this->createMock(File::class);
		$this->fileMock->eolChar = "\n";
		$fixer = $this->createMock(Fixer::class);
		$this->fileMock->fixer = $fixer;
	}

	/**
	 * Setup Config for tokenizer
	 *
	 * @return void
	 */
	private function createConfig() {
		$config = new stdClass;
		$config->annotations = true;
		$config->encoding = "utf-8";
		$config->tabWidth = 4;
		$this->config = $config;
	}

	/**
	 * Replicate Find Next
	 *
	 * @param array $args
	 * @param array $tokens
	 *
	 * @return void
	 */
	protected function findNext($args, $tokens) {
		list($types, $start, $end, $exclude, $value, $local) = $this->processArgs($args);

		$types = (array)$types;

		$numTokens = count($tokens);

		if ($end === null || $end > $numTokens) {
			$end = $numTokens;
		}

		for ($i = $start; $i < $end; $i++) {
			$found = (bool)$exclude;
			foreach ($types as $type) {
				if ($tokens[$i]['code'] === $type) {
					$found = !$exclude;
					break;
				}
			}

			if ($found === true) {
				if ($value === null) {
					return $i;
				} elseif ($tokens[$i]['content'] === $value) {
					return $i;
				}
			}

			if ($local === true && $tokens[$i]['code'] === T_SEMICOLON) {
				break;
			}
		}

		return false;
	}

	/**
	 * Replicate Find Next
	 *
	 * @param array $args
	 * @param array $tokens
	 *
	 * @return void
	 */
	protected function findPrevious($args, $tokens) {
		list($types, $start, $end, $exclude, $value, $local) = $this->processArgs($args);

		$types = (array)$types;

		if ($end === null) {
			$end = 0;
		}

		for ($i = $start; $i >= $end; $i--) {
			$found = (bool)$exclude;
			foreach ($types as $type) {
				if ($tokens[$i]['code'] === $type) {
					$found = !$exclude;
					break;
				}
			}

			if ($found === true) {
				if ($value === null) {
					return $i;
				} elseif ($tokens[$i]['content'] === $value) {
					return $i;
				}
			}

			if ($local === true) {
				if (isset($tokens[$i]['scope_opener']) === true
					&& $i === $tokens[$i]['scope_closer']
				) {
					$i = $tokens[$i]['scope_opener'];
				} elseif (isset($tokens[$i]['bracket_opener']) === true
					&& $i === $tokens[$i]['bracket_closer']
				) {
					$i = $tokens[$i]['bracket_opener'];
				} elseif (isset($tokens[$i]['parenthesis_opener']) === true
					&& $i === $tokens[$i]['parenthesis_closer']
				) {
					$i = $tokens[$i]['parenthesis_opener'];
				} elseif ($tokens[$i]['code'] === T_SEMICOLON) {
					break;
				}
			}
		}

		return false;
	}

	/**
	 * Break args into variables needed findNext
	 *
	 * @param array $args
	 *
	 * @return array
	 */
	protected function processArgs($args) {
		$types   = $args[0];
		$start   = $args[1];
		$end     = isset($args[2]) ? $args[2] : null;
		$exclude = isset($args[3]) ? $args[3] : false;
		$value   = isset($args[4]) ? $args[4] : null;
		$local   = isset($args[5]) ? $args[5] : false;

		return [$types, $start, $end, $exclude, $value, $local];
	}
}
