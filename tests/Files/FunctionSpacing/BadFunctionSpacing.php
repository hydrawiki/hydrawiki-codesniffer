<?php
/**
 * Curse Inc.
 *
 * @package   HydraWiki
 * @author    Samuel Hilson <shhilson@curse.com>
 * @copyright 2019 Curse, inc.
 * @license   MIT
 */

namespace HydraWiki;

class BadFunctionSpacing {

	/**
	 * Comment Block
	 *
	 * @return void
	 */
	public function functionFirst() {
		//
	}
	/**
	 * Comment Block
	 *
	 * @return void
	 */
	public function functionMiddle() {
		//
	}

	/**
	 * Comment Block
	 *
	 * @return void
	 */
	public function functionLast() {
		//
	}

}
