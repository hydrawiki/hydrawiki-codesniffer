<?php
/**
 * Curse Inc.
 * Parses and verifies the file doc comment.
 *
 * @package   HydraWiki
 * @author    Samuel Hilson <shhilson@curse.com>
 * @copyright 2019 Curse, inc.
 * @license   MIT
 */

namespace HydraWiki\Sniffs\Commenting;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Standards\PEAR\Sniffs\Commenting\FileCommentSniff as PearFileCommentSniff;

class FileCommentSniff extends PearFileCommentSniff {
	/**
	 * Tags in correct order and related info.
	 *
	 * @var array
	 */
	protected $tags = [
		'@category'   => [
			'required'       => false,
			'allow_multiple' => false,
		],
		'@package'    => [
			'required'       => true,
			'allow_multiple' => false,
		],
		'@subpackage' => [
			'required'       => false,
			'allow_multiple' => false,
		],
		'@author'     => [
			'required'       => true,
			'allow_multiple' => true,
		],
		'@copyright'  => [
			'required'       => false,
			'allow_multiple' => true,
		],
		'@license'    => [
			'required'       => true,
			'allow_multiple' => false,
		],
		'@version'    => [
			'required'       => false,
			'allow_multiple' => false,
		],
		'@link'       => [
			'required'       => false,
			'allow_multiple' => true,
		],
		'@see'        => [
			'required'       => false,
			'allow_multiple' => true,
		],
		'@since'      => [
			'required'       => false,
			'allow_multiple' => false,
		],
		'@deprecated' => [
			'required'       => false,
			'allow_multiple' => false,
		],
	];

	/**
	 * Processes this test, when one of its tokens is encountered.
	 *
	 * @param File    $phpcsFile
	 * @param integer $stackPtr
	 *
	 * @return void
	 */
	public function process(File $phpcsFile, $stackPtr) {
		$tokens       = $phpcsFile->getTokens();
		$commentStart = $phpcsFile->findNext(T_WHITESPACE, ($stackPtr + 1), null, true);
		if ($commentStart !== false
			&& $tokens[$commentStart]['code'] === T_DOC_COMMENT_OPEN_TAG) {
			$commentEnd = $tokens[$commentStart]['comment_closer'];

			// No blank line between the open tag and the file comment.
			$this->noBlankLineBefore($phpcsFile, $stackPtr, $tokens, $commentStart);

			// Exactly one blank line after the file comment.
			$this->oneBlankLineAfter($phpcsFile, $tokens, $commentEnd);
		}
		return parent::process($phpcsFile, $stackPtr);
	}

	/**
	 * Check for blank lines before file comment
	 *
	 * @param File    $phpcsFile
	 * @param integer $stackPtr
	 * @param array   $tokens
	 * @param integer $commentStart
	 *
	 * @return void
	 */
	public function noBlankLineBefore($phpcsFile, $stackPtr, $tokens, $commentStart) {
		if ($tokens[$commentStart]['line'] <= ($tokens[$stackPtr]['line'] + 1)) {
			return;
		}
		$error = 'There must be no blank lines before the file comment';
		$fix = $phpcsFile->addFixableError($error, $stackPtr, 'SpacingAfterOpen');
		if ($fix === true) {
			$phpcsFile->fixer->replaceToken($commentStart - 1, '');
		}
	}

	/**
	 * Check for blank lines after file comment
	 *
	 * @param File    $phpcsFile
	 * @param array   $tokens
	 * @param integer $commentEnd
	 *
	 * @return void
	 */
	public function oneBlankLineAfter($phpcsFile, $tokens, $commentEnd) {
		$next = $phpcsFile->findNext(T_WHITESPACE, ($commentEnd + 1), null, true);
		if ($tokens[$next]['line'] === ($tokens[$commentEnd]['line'] + 2)) {
			return;
		}
		$error = 'There must be exactly one blank line after the file comment';
		$fix = $phpcsFile->addFixableError($error, $commentEnd, 'SpacingAfterComment');
		if ($fix === true) {
			// Remove extra spaces
			if ($next === false) {
				$phpcsFile->fixer->replaceToken($next - 1, '');
				return;
			}
			// Add Space
			$phpcsFile->fixer->addContentBefore($commentEnd + 1, $phpcsFile->eolChar);
		}
	}

	/**
	 * Process the license tag.
	 *
	 * @param File  $phpcsFile The file being scanned.
	 * @param array $tags      The tokens for these tags.
	 *
	 * @return void
	 */
	protected function processLicense($phpcsFile, array $tags) {
		// remove functionality
	}
}
