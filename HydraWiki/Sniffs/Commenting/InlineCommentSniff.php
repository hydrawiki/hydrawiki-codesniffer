<?php
/**
 * Curse Inc.
 *
 * @package   HydraWiki
 * @author    Samuel Hilson <shhilson@curse.com>
 * @copyright 2019 Curse, inc.
 * @license   MIT
 */

namespace HydraWiki\Sniffs\Commenting;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Standards\PEAR\Sniffs\Commenting\InlineCommentSniff as PearInlineCommentSniff;

class InlineCommentSniff extends PearInlineCommentSniff {
	/**
	 * Processes this test, when one of its tokens is encountered.
	 *
	 * @param File    $phpcsFile
	 * @param integer $stackPtr
	 *
	 * @return void
	 */
	public function process(File $phpcsFile, $stackPtr) {
		$tokens = $phpcsFile->getTokens();
		$currToken = $tokens[$stackPtr];
		$preToken = $phpcsFile->findPrevious(T_WHITESPACE, $stackPtr - 1, null, true);

		// Test if comment starts on a new line
		if ($preToken !== false &&
			$tokens[$preToken]['line'] === $tokens[$stackPtr]['line']
		) {
			$phpcsFile->addWarning(
				'Comments should start on new line.',
				$stackPtr,
				'NewLineComment'
			);
		}

		// Validate Current Token
		if (!$this->isComment($currToken) ||
			$this->isDocBlockComment($currToken) ||
			$this->isEmptyComment($currToken)
		) {
			return;
		}

		// Checking whether there is a space between the comment delimiter
		// and the comment
		if (substr($currToken['content'], 0, 2) === '//') {
			$this->handleDoubleSlashComment($phpcsFile, $stackPtr, $currToken);
			return;
		}

		return parent::process($phpcsFile, $stackPtr);
	}

	/**
	 * Check contents of current token for empty state
	 *
	 * @param array $currToken
	 *
	 * @return boolean
	 */
	private function isEmptyComment($currToken) {
		return (
			(substr($currToken['content'], 0, 2) === '//' && rtrim($currToken['content']) === '//') ||
			($currToken['content'][0] === '#' && rtrim($currToken['content']) === '#')
		);
	}

	/**
	 * Check contents of current token for doc block
	 *
	 * @param array $currToken
	 *
	 * @return boolean
	 */
	private function isDocBlockComment($currToken) {
		// Accounting for multiple line comments, as single line comments
		// use only '//' and '#'
		// Also ignoring PHPDoc comments starting with '///',
		// as there are no coding standards documented for these
		return (substr($currToken['content'], 0, 2) === '/*' || substr($currToken['content'], 0, 3) === '///');
	}

	/**
	 * Check if current token is a comment token
	 *
	 * @param [type] $currToken
	 *
	 * @return boolean
	 */
	private function isComment($currToken) {
		return $currToken['code'] === T_COMMENT;
	}

	/**
	 * Handle any double slash  style'//' comments
	 *
	 * @param File    $phpcsFile
	 * @param integer $stackPtr
	 * @param array   $currToken
	 *
	 * @return void
	 */
	private function handleDoubleSlashComment($phpcsFile, $stackPtr, $currToken) {
		$commentContent = substr($currToken['content'], 2);
		$commentTrim = ltrim($commentContent);
		if (strlen($commentContent) == strlen($commentTrim)) {
			$fix = $phpcsFile->addFixableWarning(
				'At least a single space expected after "//"',
				$stackPtr,
				'SingleSpaceBeforeSingleLineComment'
			);
			if ($fix) {
				$newContent = '// ';
				$newContent .= $commentTrim;
				$phpcsFile->fixer->replaceToken($stackPtr, $newContent);
			}
		}
	}
}
